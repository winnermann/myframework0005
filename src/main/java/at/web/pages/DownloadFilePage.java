package at.web.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class DownloadFilePage {
    //Ссылка для загрузки файла
    private SelenideElement downloadLink = $(By.xpath("//a[contains(text(),'some-file.txt')]"));

    public SelenideElement getDownloadLink() {
        return downloadLink;
    }

    public void setDownloadLink(SelenideElement downloadLink) {
        this.downloadLink = downloadLink;
    }
}
