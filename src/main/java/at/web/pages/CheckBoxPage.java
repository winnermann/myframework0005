package at.web.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.element;

/**
 * PageObject
 * Страница CheckBoxPage с элементами
 *
 */
public class CheckBoxPage {

    //Поле Checkbox1
    private SelenideElement checkbox1OnCheckBoxPage = element(By.cssSelector("#checkboxes input[type=checkbox]:nth-child(1)"));
    //Поле Checkbox2
    private SelenideElement checkbox2OnCheckBoxPage = element(By.cssSelector("#checkboxes input[type=checkbox]:nth-child(3)"));


    /**
     * Действия с полем Checkbox1
     */
    public SelenideElement getCheckbox1OnCheckBoxPage() {
        return checkbox1OnCheckBoxPage;
    }

    public void setCheckbox1OnCheckBoxPage(SelenideElement checkbox1OnCheckBoxPage) {
        this.checkbox1OnCheckBoxPage = checkbox1OnCheckBoxPage;
    }

    public SelenideElement getCheckbox2OnCheckBoxPage() {
        return checkbox2OnCheckBoxPage;
    }

    public void setCheckbox2OnCheckBoxPage(SelenideElement checkbox2OnCheckBoxPage) {
        this.checkbox2OnCheckBoxPage = checkbox2OnCheckBoxPage;
    }
}
