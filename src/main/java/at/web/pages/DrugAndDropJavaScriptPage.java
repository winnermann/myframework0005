package at.web.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

/**
 * PageObject
 * Страница DrugAndDropJavaScriptPage с элементами
 */
public class DrugAndDropJavaScriptPage {
    //Колонка А
    private SelenideElement columnAOnDrugAndDropJavaScriptPage = $("#column-a");
    //Колонка B
    private SelenideElement columnBOnDrugAndDropJavaScriptPage = $("#column-b");

    public SelenideElement getColumnAOnDrugAndDropJavaScriptPage() {
        return columnAOnDrugAndDropJavaScriptPage;
    }

    public void setColumnAOnDrugAndDropJavaScriptPage(SelenideElement columnAOnDrugAndDropJavaScriptPage) {
        this.columnAOnDrugAndDropJavaScriptPage = columnAOnDrugAndDropJavaScriptPage;
    }

    public SelenideElement getColumnBOnDrugAndDropJavaScriptPage() {
        return columnBOnDrugAndDropJavaScriptPage;
    }

    public void setColumnBOnDrugAndDropJavaScriptPage(SelenideElement columnBOnDrugAndDropJavaScriptPage) {
        this.columnBOnDrugAndDropJavaScriptPage = columnBOnDrugAndDropJavaScriptPage;
    }
}
