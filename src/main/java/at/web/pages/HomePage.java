package at.web.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.element;

/**
 * PageObject
 * Страница HomePage с элементами
 * textWelcomeOnHomePage - Слова "Welcome to the-internet" на странице "HomePage"
 */
public class HomePage {
    //Слова "Form Authentication" на странице "HomePage"
    private SelenideElement textFormAuthenticationOnHomePage = element(By.cssSelector("#content li:nth-child(21) a"));
    //Слова "Welcome to the-internet" на странице "HomePage"
    private SelenideElement textWelcomeOnHomePage = element(By.xpath("//h1[@class=\"heading\"]"));
    //Слова "Checkboxes" на странице "HomePage"
    private SelenideElement textCheckboxesOnHomePage = element(By.cssSelector("#content li:nth-child(6) a"));
    //Слова "Dropdown" на странице "HomePage"
    private SelenideElement textDropdownOnHomePage = element(By.cssSelector("#content li:nth-child(11) a"));
    //Слова "DragAndDrop" на странице "HomePage"
    private SelenideElement textDragAndDropOnHomePage = element(By.cssSelector("#content li:nth-child(10) a"));
    //Слова "File Download" на странице "HomePage"
    private SelenideElement textFileDownloadOnHomePage = element(By.cssSelector("#content li:nth-child(17) a"));

    public SelenideElement getTextFormAuthenticationOnHomePage() {
        return textFormAuthenticationOnHomePage;
    }

    public void setTextFormAuthenticationOnHomePage(SelenideElement textFormAuthenticationOnHomePage) {
        this.textFormAuthenticationOnHomePage = textFormAuthenticationOnHomePage;
    }

    public SelenideElement getTextWelcomeOnHomePage() {
        return textWelcomeOnHomePage;
    }

    public void setTextWelcomeOnHomePage(SelenideElement textWelcomeOnHomePage) {
        this.textWelcomeOnHomePage = textWelcomeOnHomePage;
    }

    public SelenideElement getTextCheckboxesOnHomePage() {
        return textCheckboxesOnHomePage;
    }

    public void setTextCheckboxesOnHomePage(SelenideElement textCheckboxesOnHomePage) {
        this.textCheckboxesOnHomePage = textCheckboxesOnHomePage;
    }

    public SelenideElement getTextDropdownOnHomePage() {
        return textDropdownOnHomePage;
    }

    public void setTextDropdownOnHomePage(SelenideElement textDropdownOnHomePage) {
        this.textDropdownOnHomePage = textDropdownOnHomePage;
    }

    public SelenideElement getTextDragAndDropOnHomePage() {
        return textDragAndDropOnHomePage;
    }

    public void setTextDragAndDropOnHomePage(SelenideElement textDragAndDropOnHomePage) {
        this.textDragAndDropOnHomePage = textDragAndDropOnHomePage;
    }

    public SelenideElement getTextFileDownloadOnHomePage() {
        return textFileDownloadOnHomePage;
    }

    public void setTextFileDownloadOnHomePage(SelenideElement textFileDownloadOnHomePage) {
        this.textFileDownloadOnHomePage = textFileDownloadOnHomePage;
    }
}
