package at.web.pages;

import com.codeborne.selenide.ElementsCollection;

import static com.codeborne.selenide.Selenide.$$x;

/**
 * Страница с коллекцией элементов
 */
public class DownloadCollectionStreamApiLambda {
    /**
     * ArrayList с элементами для скачивания
     */
    private ElementsCollection hrefsOnDownloadCollectionStreamApiLambda = $$x("//div[@class='example']//a[@href]");

    public ElementsCollection getHrefsOnDownloadCollectionStreamApiLambda() {
        return hrefsOnDownloadCollectionStreamApiLambda;
    }

    public void setHrefsOnDownloadCollectionStreamApiLambda(ElementsCollection hrefsOnDownloadCollectionStreamApiLambda) {
        this.hrefsOnDownloadCollectionStreamApiLambda = hrefsOnDownloadCollectionStreamApiLambda;
    }
}
