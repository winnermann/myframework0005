package at.web.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.element;

public class LoginPage {
    private SelenideElement loginFieldOnLoginPage = element(By.xpath("//input[@id='username']"));
    private SelenideElement passwordFieldOnLoginPage = element(By.xpath("//input[@id='password']"));
    private SelenideElement loginButtonOnLoginPage = element(By.xpath("//button[@class='radius']"));

    public SelenideElement getLoginFieldOnLoginPage() {
        return loginFieldOnLoginPage;
    }

    public void setLoginFieldOnLoginPage(SelenideElement loginFieldOnLoginPage) {
        this.loginFieldOnLoginPage = loginFieldOnLoginPage;
    }

    public SelenideElement getPasswordFieldOnLoginPage() {
        return passwordFieldOnLoginPage;
    }

    public void setPasswordFieldOnLoginPage(SelenideElement passwordFieldOnLoginPage) {
        this.passwordFieldOnLoginPage = passwordFieldOnLoginPage;
    }

    public SelenideElement getLoginButtonOnLoginPage() {
        return loginButtonOnLoginPage;
    }

    public void setLoginButtonOnLoginPage(SelenideElement loginButtonOnLoginPage) {
        this.loginButtonOnLoginPage = loginButtonOnLoginPage;
    }
}
