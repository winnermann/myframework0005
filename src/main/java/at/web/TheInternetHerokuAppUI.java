package at.web;
import at.web.pages.*;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Condition.checked;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TheInternetHerokuAppUI {
    static LoginPage loginPage = new LoginPage();
    static HomePage homePage = new HomePage();
    static CheckBoxPage checkBoxPage = new CheckBoxPage();
    static DropdownMenuPage dropdownMenuPage = new DropdownMenuPage();
    static DrugAndDropJavaScriptPage drugAndDropJavaScriptPage = new DrugAndDropJavaScriptPage();
    static DownloadFilePage downloadFilePage = new DownloadFilePage();
    static DownloadCollectionStreamApiLambda downloadCollectionStreamApiLambda = new DownloadCollectionStreamApiLambda();

    public static void startBrowser(){
        //Открыть браузер
        //String driverPath = "/builds/winnermann/myframework0005/chromedriver";
        //String driverPath = "chromedriver.exe";

        //String driverPath = "chromedriver";
        //System.setProperty("webdriver.chrome.driver", driverPath);
        System.setProperty("selenide.browser", "chrome");
        Configuration.headless=false;
        Configuration.driverManagerEnabled = true;
        Configuration.browser = "chrome";
        Configuration.startMaximized = true;
        Configuration.timeout = 6000;
        open("http://the-internet.herokuapp.com");
        //Выводит  заголовок страницы в консоль
        System.out.println(Selenide.title());
        //Проверяет, что заголовок страницы правильный
        assertTrue(Selenide.title().equals("The Internet"));

    }

    @Step("Login")
    public static void login(){
        //Убедиться что на странице есть слова "Welcome to the-internet"
        assertEquals(homePage.getTextWelcomeOnHomePage().getOwnText(), "Welcome to the-internet");
        //Убедиться что ссылка содержит слова "Form Authentication"
        assertEquals(homePage.getTextFormAuthenticationOnHomePage().getOwnText(), "Form Authentication");
        //перейти по ссылке "Form Authentication"
        homePage.getTextFormAuthenticationOnHomePage().click();

        loginPage.getLoginFieldOnLoginPage().setValue("tomsmith");
        loginPage.getPasswordFieldOnLoginPage().val("SuperSecretPassword!");
        loginPage.getLoginButtonOnLoginPage().click();

    }

    @Step("checkBox")
    public static void checkBox(){
        //Убедиться что на странице есть слова "Welcome to the-internet"
        assertEquals(homePage.getTextWelcomeOnHomePage().getOwnText(), "Welcome to the-internet");
        //Убедиться что ссылка содержит слова "Checkboxes"
        assertEquals(homePage.getTextCheckboxesOnHomePage().getOwnText(), "Checkboxes");
        //перейти по ссылке "Checkboxes"
        homePage.getTextCheckboxesOnHomePage().click();

        //Проверяет что Чек-бокс не выбран
        checkBoxPage.getCheckbox1OnCheckBoxPage().shouldNotBe(checked);
        //Проставляет Чек-бокс
        checkBoxPage.getCheckbox1OnCheckBoxPage().setSelected(true);
        //Проверяет что Чек-бокс выбран
        checkBoxPage.getCheckbox1OnCheckBoxPage().shouldBe(checked);

        //Проверяет что Чек-бокс выбран
        checkBoxPage.getCheckbox2OnCheckBoxPage().shouldBe(checked);
        //Снимает Чек-бокс
        checkBoxPage.getCheckbox2OnCheckBoxPage().setSelected(false);
        //Проверяет что Чек-бокс не выбран
        checkBoxPage.getCheckbox2OnCheckBoxPage().shouldNotBe(checked);

    }

    @Step("dropDownMenu")
    public static void dropDownMenu(){
        //Открыть браузер
        startBrowser();

        //Убедиться что на странице есть слова "Welcome to the-internet"
        assertEquals(homePage.getTextWelcomeOnHomePage().getOwnText(), "Welcome to the-internet");
        //Убедиться что ссылка содержит слова "Checkboxes"
        assertEquals(homePage.getTextDropdownOnHomePage().getOwnText(), "Dropdown");
        //перейти по ссылке "Checkboxes"
        homePage.getTextDropdownOnHomePage().click();


        //Выбрать из выпадающего меню Option 2
        //dropdownMenuPage.getParentDiv("#dropdown").find("option:nth-child(3)").scrollTo().click();
        dropdownMenuPage.getParentDiv("#dropdown").selectOption(2);

        //проверяет что меню Option 2 выбрано
        //dropdownMenuPage.getParentDiv("#dropdown").find("option:nth-child(3)").shouldHave(text("Option 2"));
        assertEquals(dropdownMenuPage.getParentDiv("#dropdown").getSelectedOption().getOwnText(), "Option 2");


        //Выбрать из выпадающего меню Option 1
        //dropdownMenuPage.getParentDiv("#dropdown").find("option:nth-child(2)").scrollTo().click();
        dropdownMenuPage.getParentDiv("#dropdown").selectOptionByValue(String.valueOf(1));

        //проверяет что меню Option 1 выбрано
        //dropdownMenuPage.getParentDiv("#dropdown").find("option:nth-child(2)").shouldHave(text("Option 1"));
        assertEquals(dropdownMenuPage.getParentDiv("#dropdown").getSelectedOption().getOwnText(), "Option 1");
    }

    @Step("drugAndDropJavaScript")
    public static void drugAndDropJavaScript() throws IOException {
        //Открыть браузер
        startBrowser();

        //Убедиться что на странице есть слова "Welcome to the-internet"
        assertEquals(homePage.getTextWelcomeOnHomePage().getOwnText(), "Welcome to the-internet");
        //Убедиться что ссылка содержит слова "Drag and Drop"
        assertEquals(homePage.getTextDragAndDropOnHomePage().getOwnText(), "Drag and Drop");
        //перейти по ссылке "Drag and Drop"
        homePage.getTextDragAndDropOnHomePage().click();


        //Проверяет что элемент КолонкаА содержит текст А
        //element(By.cssSelector("#column-a")).shouldHave(text("A"));
        //drugAndDropJavaScriptPage.shouldCheckColumnATextOnDrugAndDropJavaScriptPage("A");
        drugAndDropJavaScriptPage.getColumnAOnDrugAndDropJavaScriptPage().shouldHave(text("A"));
        //assertEquals(drugAndDropJavaScriptPage.getColumnAOnDrugAndDropJavaScriptPage().getOwnText(), "A");

        //Создадим объект класса File
        File dnd_javascript = new File("scripts/dnd.js");
        FileReader reader = new FileReader(dnd_javascript);
        BufferedReader bufferedReader = new BufferedReader(reader);
        StringBuilder builder = new StringBuilder();
        bufferedReader.lines().forEach(
                o->builder.append(o).append('\n')
        );

        String javaScript = builder.toString();
        javaScript = javaScript + " simulateDragDrop(document.getElementById('column-a'),document.getElementById('column-b'));";
        Selenide.executeJavaScript(javaScript);

        //Проверяет что элемент КолонкаА содержит текст Б
        //element(By.cssSelector("#column-a")).shouldHave(text("B"));
        //drugAndDropJavaScriptPage.shouldCheckColumnATextOnDrugAndDropJavaScriptPage("B");
        //assertEquals(drugAndDropJavaScriptPage.getColumnAOnDrugAndDropJavaScriptPage().getOwnText(), "B");
        drugAndDropJavaScriptPage.getColumnAOnDrugAndDropJavaScriptPage().shouldHave(text("B"));

    }

    @Step("downloadFile")
    public static void downloadFile() throws IOException {
        //Открыть браузер
        startBrowser();

        //Убедиться что на странице есть слова "Welcome to the-internet"
        assertEquals(homePage.getTextWelcomeOnHomePage().getOwnText(), "Welcome to the-internet");
        //Убедиться что ссылка содержит слова "File Download"
        assertEquals(homePage.getTextFileDownloadOnHomePage().getOwnText(), "File Download");
        //перейти по ссылке "File Download"
        homePage.getTextFileDownloadOnHomePage().click();

        //Производит загрузку файла some-file.txt с сервера в папку build/downloads
        //File report = $(By.xpath("//a[contains(text(),'some-file.txt')]")).download();
        File report = downloadFilePage.getDownloadLink().download();

        //Проверяет, что нужный файл скачан с сервера
        assertEquals(report.getName(), "some-file.txt");

        //Выводит в консоль весь путь до файла
        System.out.println(report + " Путь до файла");
        //Выводит в консоль только название файла
        System.out.println(report.getName()+" Имя файла");

        //Удаляет папку downloads с загруженным файлом some-file.txt
        FileUtils.deleteDirectory(new File("build/downloads"));

    }

    @Step("downloadCollectionForCycle")
    public static void downloadCollectionForCycle() throws FileNotFoundException {
        //Открыть браузер
        startBrowser();

        //Убедиться что на странице есть слова "Welcome to the-internet"
        assertEquals(homePage.getTextWelcomeOnHomePage().getOwnText(), "Welcome to the-internet");
        //Убедиться что ссылка содержит слова "File Download"
        assertEquals(homePage.getTextFileDownloadOnHomePage().getOwnText(), "File Download");
        //перейти по ссылке "File Download"
        homePage.getTextFileDownloadOnHomePage().click();


        ElementsCollection hrefs = $$x("//div[@class='example']//a[@href]");
        List<String> links = new ArrayList<>();
        for (int i = 0; i <hrefs.size() ; i++) {
            links.add(hrefs.get(i).getAttribute("href"));

        }
        //File report = $(By.xpath("//a[contains(text(),'some-file.txt')]")).download();
        //String report2 = links.get(44);

    }

    @Step("downloadCollectionForEachCycle")
    public static void downloadCollectionForEachCycle(){
        //Открыть браузер
        startBrowser();

        //Убедиться что на странице есть слова "Welcome to the-internet"
        assertEquals(homePage.getTextWelcomeOnHomePage().getOwnText(), "Welcome to the-internet");
        //Убедиться что ссылка содержит слова "File Download"
        assertEquals(homePage.getTextFileDownloadOnHomePage().getOwnText(), "File Download");
        //перейти по ссылке "File Download"
        homePage.getTextFileDownloadOnHomePage().click();


        ElementsCollection hrefs = $$x("//div[@class='example']//a[@href]");
        List<String> links = new ArrayList<>();
        for (SelenideElement element : hrefs) {
            links.add(element.getAttribute("href"));

        }
        int b = 0;

    }

    @Step("downloadCollectionStreamApiLambda")
    public static void downloadCollectionStreamApiLambda() throws IOException {
        //Открыть браузер
        startBrowser();

        //Убедиться что на странице есть слова "Welcome to the-internet"
        assertEquals(homePage.getTextWelcomeOnHomePage().getOwnText(), "Welcome to the-internet");
        //Убедиться что ссылка содержит слова "File Download"
        assertEquals(homePage.getTextFileDownloadOnHomePage().getOwnText(), "File Download");
        //перейти по ссылке "File Download"
        homePage.getTextFileDownloadOnHomePage().click();

        //Производит загрузку файла с индексом get(20) с сервера в папку build/downloads
        File report = downloadCollectionStreamApiLambda.getHrefsOnDownloadCollectionStreamApiLambda().get(20).download();

        //Проверяет, что нужный файл скачан с сервера
        assertEquals("some-file.txt", report.getName());

        //Выводит в консоль весь путь до файла
        System.out.println(report + " Путь до файла");
        //Выводит в консоль только название файла
        System.out.println(report.getName()+" Имя файла");

    }

    @Step("downloadCollectionStreamApiLambda2")
    public static void downloadCollectionStreamApiLambda2() throws IOException {
        //Открыть браузер
        startBrowser();

        //Убедиться что на странице есть слова "Welcome to the-internet"
        assertEquals(homePage.getTextWelcomeOnHomePage().getOwnText(), "Welcome to the-internet");
        //Убедиться что ссылка содержит слова "File Download"
        assertEquals(homePage.getTextFileDownloadOnHomePage().getOwnText(), "File Download");
        //перейти по ссылке "File Download"
        homePage.getTextFileDownloadOnHomePage().click();


        //ElementsCollection hrefs = $$x("//div[@class='example']//a[@href]");
        //List<String> links = new ArrayList<>();

        //Stream API + Lambda
        //hrefs.stream().forEach(x->links.add(x.getAttribute("href")));
        //hrefs.forEach(x->links.add(x.getAttribute("href"))); //более коротко, убрали stream()
        //downloadCollectionStreamApiLambda.getHrefsOnDownloadCollectionStreamApiLambda().forEach(x->links.add(x.getAttribute("href")));
        //File report = $(By.xpath("//a[contains(text(),'some-file.txt')]")).download();

        //Кликает по ссылке с индексом get(20). По этой ссылке скачивается файл.
        downloadCollectionStreamApiLambda.getHrefsOnDownloadCollectionStreamApiLambda().get(20).click();

        //Проверяет, что нужный файл скачан с сервера
        //assertEquals("some-file.txt", downloadCollectionStreamApiLambda.getHrefsOnDownloadCollectionStreamApiLambda().get(20).getText());
        assertTrue(downloadCollectionStreamApiLambda.getHrefsOnDownloadCollectionStreamApiLambda().get(20).getText().equals("some-file.txt"));

        //Выводит в консоль весь путь до файла
        System.out.println(downloadCollectionStreamApiLambda.getHrefsOnDownloadCollectionStreamApiLambda().get(20).getOwnText());

        //Выводит в консоль только название файла
        System.out.println(downloadCollectionStreamApiLambda.getHrefsOnDownloadCollectionStreamApiLambda().get(20).getText() +" Имя файла");

    }



    public static void closeBrowser() throws IOException {
        //Удаляет папку downloads с загруженным файлом some-file.txt
        FileUtils.deleteDirectory(new File("build/downloads"));
        Selenide.clearBrowserCookies();
        Selenide.closeWindow();
    }
}
