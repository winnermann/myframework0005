package at.web;

import org.junit.jupiter.api.*;

import java.io.IOException;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TheInternetHerokuAppUITest {
    @BeforeAll
    public static void startBrowser(){
        System.out.println("startBrowser");
        TheInternetHerokuAppUI.startBrowser();
    }

    @Order(1)
    @Test
    @DisplayName("Cценарий: login")
    public void login() {
        TheInternetHerokuAppUI.login();

    }

//    @Order(2)
//    @Test
//    @DisplayName("Cценарий: checkBox")
//    public void checkBox() {
//        TheInternetHerokuAppUI.checkBox();
//
//    }
//
//    @Order(3)
//    @Test
//    @DisplayName("Cценарий: dropDownMenu")
//    public void dropDownMenu() {
//        TheInternetHerokuAppUI.dropDownMenu();
//
//    }
//
//    @Order(4)
//    @Test
//    @DisplayName("Cценарий: drugAndDropJavaScript")
//    public void drugAndDropJavaScript() throws IOException {
//        TheInternetHerokuAppUI.drugAndDropJavaScript();
//
//    }
//
//    @Order(5)
//    @Test
//    @DisplayName("Cценарий: downloadFile")
//    public void downloadFile() throws IOException {
//        TheInternetHerokuAppUI.downloadFile();
//
//    }
//
//    @Order(6)
//    @Test
//    @DisplayName("Cценарий: downloadCollectionForCycle")
//    public void downloadCollectionForCycle() throws IOException {
//        TheInternetHerokuAppUI.downloadCollectionForCycle();
//
//    }
//
//    @Order(7)
//    @Test
//    @DisplayName("Cценарий: downloadCollectionForEachCycle")
//    public void downloadCollectionForEachCycle() throws IOException {
//        TheInternetHerokuAppUI.downloadCollectionForEachCycle();
//
//    }
//
//    @Order(8)
//    @Test
//    @DisplayName("Cценарий: downloadCollectionStreamApiLambda")
//    public void downloadCollectionStreamApiLambda() throws IOException {
//        TheInternetHerokuAppUI.downloadCollectionStreamApiLambda();
//
//    }
//
//    @Order(9)
//    @Test
//    @DisplayName("Cценарий: downloadCollectionStreamApiLambda2")
//    public void downloadCollectionStreamApiLambda2() throws IOException {
//        TheInternetHerokuAppUI.downloadCollectionStreamApiLambda2();
//
//    }

    @AfterAll
    public static void closeBrowser() throws IOException {
        TheInternetHerokuAppUI.closeBrowser();
        System.out.println("closeBrowser");

    }
}
